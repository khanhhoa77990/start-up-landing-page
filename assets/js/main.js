(function ($, window) {
    $(document).ready(function () {
        const header = $('.header');

        $(window).on("scroll", function () {
            let scroll = $(window).scrollTop();

            if (scroll > header.outerHeight()) {
                header.addClass("shadow");
            }
            else {
                header.removeClass("shadow");
            }
        });
    });
})(jQuery, window);